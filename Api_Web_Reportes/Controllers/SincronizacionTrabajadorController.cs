﻿using Api_Web_Reportes.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_Web_Reportes.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/SincronizacionTrabajador")]
    public class SincronizacionTrabajadorController : ApiController
    {
        //[Authorize]
        [HttpPost]
        [Route("ListarTrabajadores")]
        public IHttpActionResult ListarTrabajadores()
        {
            Dao dao;
            
            try
            {
                dao = new Dao();

                var _Response = dao.ListarTrabajadores();

                return Ok(new ResponseSincronizacion { Status = 1, Response = _Response });

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message});
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_TrabajadorToSql07")]
        public IHttpActionResult InsertUpdate_TrabajadorToSql07(Trabajador_Model Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = dao.InsertUpdate_TrabajadorToSql07(Entry);

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_TrabajadorToSiderPrd01")]
        public IHttpActionResult InsertUpdate_TrabajadorToSiderPrd01(Trabajador_Model Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = dao.InsertUpdate_TrabajadorToSiderPrd01(Entry);

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("ListarGerencias")]
        public IHttpActionResult ListarGerencias()
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = dao.ListarGerencias();

                return Ok(new ResponseSincronizacion { Status = 1, Response = _Response });

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("ListarAreas")]
        public IHttpActionResult ListarAreas()
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = dao.ListarAreas();

                return Ok(new ResponseSincronizacion { Status = 1, Response = _Response });

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("ListarSubareas")]
        public IHttpActionResult ListarSubareas()
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = dao.ListarSubareas();

                return Ok(new ResponseSincronizacion { Status = 1, Response = _Response });

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("ListarCelulas")]
        public IHttpActionResult ListarCelulas()
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = dao.ListarCelulas();

                return Ok(new ResponseSincronizacion { Status = 1, Response = _Response });

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_GerenciasToSql07")]
        public IHttpActionResult InsertUpdate_GerenciasToSql07(List<Gerencias_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();
                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Gerencias_Model Entity in Entry)
                {
                   _Response = dao.InsertUpdate_GerenciasToSql07(Entity);
                }                 

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_AreasToSql07")]
        public IHttpActionResult InsertUpdate_AreasToSql07(List<Areas_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();
                
                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Areas_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_AreasToSql07(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_SubareasToSql07")]
        public IHttpActionResult InsertUpdate_SubareasToSql07(List<Subareas_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Subareas_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_SubareasToSql07(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_CelulasToSql07")]
        public IHttpActionResult InsertUpdate_CelulasToSql07(List<Celulas_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Celulas_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_CelulasToSql07(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_GerenciasToSiderPrd01")]
        public IHttpActionResult InsertUpdate_GerenciasToSiderPrd01(List<Gerencias_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Gerencias_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_GerenciasToSiderPrd01(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_AreasToSiderPrd01")]
        public IHttpActionResult InsertUpdate_AreasToSiderPrd01(List<Areas_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();

                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Areas_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_AreasToSiderPrd01(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_SubareasToSiderPrd01")]
        public IHttpActionResult InsertUpdate_SubareasToSiderPrd01(List<Subareas_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();


                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Subareas_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_SubareasToSiderPrd01(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
        //[Authorize]
        [HttpPost]
        [Route("InsertUpdate_CelulasToSiderPrd01")]
        public IHttpActionResult InsertUpdate_CelulasToSiderPrd01(List<Celulas_Model> Entry)
        {
            Dao dao;

            try
            {
                dao = new Dao();


                var _Response = new ResponseSincronizacion { Status = -1, Exception = "" };

                foreach (Celulas_Model Entity in Entry)
                {
                    _Response = dao.InsertUpdate_CelulasToSiderPrd01(Entity);
                }

                return Ok(_Response);

            }
            catch (Exception e)
            {
                return Ok(new ResponseSincronizacion { Status = -1, Exception = e.Message });
            }
        }
    }
}
