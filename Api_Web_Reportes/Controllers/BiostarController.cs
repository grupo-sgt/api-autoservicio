﻿using Api_Web_Reportes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_Web_Reportes.Controllers
{


    [AllowAnonymous]
    [RoutePrefix("api/Biostar")]
    public class BiostarController : ApiController
    {
        //[Authorize]
        [HttpGet]
        [Route("DesactivarFotocheck")]
        public IHttpActionResult EnviarRespuestaCorreo(String fichaSap)
        {
            Dao dao = new Dao();
            try
            {
                return Ok(dao.Asis_Bloquear_Fotocheck(fichaSap));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
    }
}
