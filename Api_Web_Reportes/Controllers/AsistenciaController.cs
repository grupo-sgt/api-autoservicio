﻿using Api_Web_Reportes.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_Web_Reportes.Controllers
{
        [AllowAnonymous]
        [RoutePrefix("api/Asistencia")]
        public class AsistenciaController : ApiController
        {
            //[Authorize]
            [HttpPost]
            [Route("diaAsistido")]
            public IHttpActionResult DiaAsistido(EntryDiaAsistido Entry)
            {
                Dao dao;
                List<object> l_result;
                int l_value = 0;
                string l_messsage = "";
                try
                {
                    dao = new Dao();
                    l_result = new List<object>();
                    var l_allRecords = dao.Asis_Dia_Asistido(Entry);

                    //foreach (DataRow row in l_allRecords.Rows)
                    //{
                    //    l_value = Convert.ToInt16(row["value"]);
                    //    l_messsage = row["message"].ToString();
                    //}

                    return Ok(l_allRecords);
                    //return Ok(new Response { Status = l_value, Message = l_messsage });

                }
                catch (Exception e)
                {
                    return Ok(Helpers.GetException(e));
                }
            }
        }
    
}
