﻿using Api_Web_Reportes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_Web_Reportes.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Reportes")]
    public class EnviarCorreoRespuestaController : ApiController
    {
        //[Authorize]
        [HttpPost]
        [Route("EnviarRespuestaCorreo")]
        public IHttpActionResult EnviarRespuestaCorreo(EntryCorreoMoviInt Entry)
        {
            Dao dao = new Dao();
            try
            {
                return Ok(dao.Asis_Respuesta_Correo_Movi_Int(Entry));
            }
            catch (Exception e) 
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("RecordVacacional")]
        public IHttpActionResult RecordVacacional(EntryRecord Entry)
        {
            Dao dao = new Dao();
            try
            {
                if (!ModelState.IsValid)
                {
                    // to do  :return something. May be the validation errors?
                    var errors = new List<string>();
                    foreach (var modelStateVal in ModelState.Values.Select(d => d.Errors))
                    {
                        errors.AddRange(modelStateVal.Select(error => error.ErrorMessage));
                    }
                    return Ok(new
                    {
                        Code = "3000",
                        Message = errors
                    });
                }
                return Ok(dao.Asis_Record_Vacacional(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
    }
}
