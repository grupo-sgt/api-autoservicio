using System.Web.Http;
using WebActivatorEx;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Web.Http.Description;
using Api_Web_Reportes.App_Start;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Api_Web_Reportes.App_Start
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "Api Rest Personas")
                            //.Description("Aqu� una descripci�n del Web API.")
                            //.TermsOfService("T�rminos de servicio.")
                            .Contact(x => x
                                .Name("Pedro Reque"));

                        // If you annotate Controllers and API Types with
                        // Xml comments (http://msdn.microsoft.com/en-us/library/b2s063f7(v=vs.110).aspx), you can incorporate
                        // those comments into the generated docs and UI. You can enable this by providing the path to one or
                        // more Xml comment files.
                        //
                        // HABILITAMOS EL ARCHIVO DE DOCUMENTACI�N XML.
                        //c.IncludeXmlComments(GetXmlCommentsPath());

                        // NOTE: You must also configure 'EnableApiKeySupport' below in the SwaggerUI section
                        //
                        // HABILITAMOS LA AUTENTICACI�N JWT.
                        c.ApiKey("Authorization")
                        .Description("Introduce el Token JWT aqu�.")
                        .Name("Bearer")
                        .In("header");
                        c.OperationFilter<AuthorizationHeaderParameterOperationFilter>();
                    })
                .EnableSwaggerUi();
        }
        public class AuthorizationHeaderParameterOperationFilter : IOperationFilter
        {
            public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
            {
                if (operation.parameters == null)
                    operation.parameters = new List<Parameter>();

                operation.parameters.Add(new Parameter
                {
                    name = "Authorization",
                    @in = "header",
                    description = "JWT Token",
                    required = false,
                    type = "string"
                });
            }
        }
    }
}
