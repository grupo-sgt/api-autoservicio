﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Api_Web_Reportes.Models
{
    public class Dao
    {
        public ResponseCorreoMoviInt Asis_Respuesta_Correo_Movi_Int(EntryCorreoMoviInt entry)
        {
            OracleConnection ObjConn = Helpers.ConnectToOracle("PERSONAS");
            List<ResponseCorreoMoviInt> lista = new List<ResponseCorreoMoviInt>();
            ResponseCorreoMoviInt entidad = null;
            if (ObjConn != null)
            {
                ObjConn.Open();
                OracleDataReader odr = null;
                //ResponseCorreoMoviInt entidad = null;
                OracleCommand command = ObjConn.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "SP_ASIS_RESPUESTA_CORREO_MOVI_INTERNO";
                command.Parameters.Add(new OracleParameter("ficha_sap", entry.FichaSap));
                command.Parameters.Add(new OracleParameter("codi_trab", entry.CodiTrab));
                command.Parameters.Add(new OracleParameter("codi_mov", entry.CodiMov));
                command.Parameters.Add(new OracleParameter("rpta", entry.Rpta));
                command.Parameters.Add(new OracleParameter("observaciones", entry.Observaciones));
                command.Parameters.Add(new OracleParameter("flujo", entry.Flujo));
                command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                odr = command.ExecuteReader();
                //lista = new List<ResponseCorreoMoviInt>();
                //while (odr.Read())
                //{
                //    entidad = new ResponseCorreoMoviInt
                //    {
                //        Valor = odr.IsDBNull(0) ? "" : odr.GetString(0),
                //        Mensaje = odr.IsDBNull(1) ? "" : odr.GetString(1),
                //        Html = odr.IsDBNull(2) ? "" : odr.GetString(2)
                //    };
                //    lista.Add(entidad);
                //}
                lista = new List<ResponseCorreoMoviInt>();
                while (odr.Read())
                {
                    entidad = new ResponseCorreoMoviInt
                    {
                        Valor = odr.IsDBNull(0) ? "" : odr.GetString(0),
                        Mensaje = odr.IsDBNull(1) ? "" : odr.GetString(1),
                        Html = odr.IsDBNull(2) ? "" : odr.GetString(2)
                    };
                }
                odr.Close();
                ObjConn.Close();
            }
            return entidad;
        }

        public DataTable Asis_Respuesta_Correo_vacaciones(EntryAprobacionVacacion entry)
        {
            DataTable l_dt = null;
            try
            {
                using (OracleConnection conn = Helpers.ConnectToOracle("PERSONAS"))
                using (OracleDataAdapter da = new OracleDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "SP_ASIS_APROB_VACACIONES_TRABAJADOR_EMAIL";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new OracleParameter("pn_codi_trab", entry.pn_codi_trab));
                    da.SelectCommand.Parameters.Add(new OracleParameter("pn_codi_perfil", entry.pn_codi_perfil));
                    da.SelectCommand.Parameters.Add(new OracleParameter("pn_codi_pava", entry.pn_codi_pava));
                    da.SelectCommand.Parameters.Add(new OracleParameter("pv_estado", entry.pv_estado));
                    da.SelectCommand.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable Asis_Dia_Asistido(EntryDiaAsistido entry)
        {
            DataTable l_dt = null;
            try
            {
                using (OracleConnection conn = Helpers.ConnectToOracle("PERSONAS"))
                using (OracleDataAdapter da = new OracleDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "SP_ASIS_OBT_TRABAJO_EN_DIA";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new OracleParameter("pv_ficha_sap", entry.pv_ficha_sap));
                    da.SelectCommand.Parameters.Add(new OracleParameter("pv_fecha", entry.pv_fecha));
                    da.SelectCommand.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        

        public DataTable Asis_Record_Vacacional(EntryRecord entry)
        {
            DataTable l_dt = null;
            try
            {
                using (OracleConnection conn = Helpers.ConnectToOracle("PERSONAS"))
                using (OracleDataAdapter da = new OracleDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "SP_ASIS_RPT_RECORD_VACACIONAL_API_SGT";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new OracleParameter("ANIO", entry.anio));
                    da.SelectCommand.Parameters.Add(new OracleParameter("MES", entry.mes));
                    da.SelectCommand.Parameters.Add(new OracleParameter("TIPO", "API"));
                    da.SelectCommand.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable Asis_Bloquear_Fotocheck(String fichaSap)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("Biostar"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "sp_Visi_InHabilita_Biostar_Persona";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@nUserIdn", fichaSap));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public List<Trabajador_Model> ListarTrabajadores()
        {
            try
            {
                OracleConnection ObjConn = Helpers.ConnectToOracle("PERSONAS");
                List<Trabajador_Model> List = null;
                if (ObjConn != null)
                {
                    ObjConn.Open();
                    OracleDataReader odr = null;
                    OracleCommand command = ObjConn.CreateCommand();

                    List = new List<Trabajador_Model>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Sp_List_Sincronizacion_Trabajadores";
                    //command.Parameters.Add(new OracleParameter("pn_codi_trab", entry.CodiTrab));
                    command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    odr = command.ExecuteReader();
                    while (odr.Read())
                    {
                        Trabajador_Model Trabajador = new Trabajador_Model();
                        Trabajador.CodiTrab = odr.IsDBNull(0) ? "" : odr.GetString(0);
                        Trabajador.FichaSap = odr.IsDBNull(1) ? "" : odr.GetString(1);
                        Trabajador.TarjetaHid = odr.IsDBNull(2) ? "" : odr.GetString(2);
                        Trabajador.ApellidoPaterno = odr.IsDBNull(3) ? "" : odr.GetString(3);
                        Trabajador.ApellidoMaterno = odr.IsDBNull(4) ? "" : odr.GetString(4);
                        Trabajador.Nombres = odr.IsDBNull(5) ? "" : odr.GetString(5);
                        Trabajador.FichaTareo = odr.IsDBNull(6) ? "" : odr.GetString(6);
                        Trabajador.FichaAntigua = odr.IsDBNull(7) ? "" : odr.GetString(7);
                        Trabajador.CodiGere = odr.IsDBNull(8) ? "" : odr.GetString(8);
                        Trabajador.CodiRela = odr.IsDBNull(9) ? "" : odr.GetString(9);
                        Trabajador.CodiMoco = odr.IsDBNull(10) ? "" : odr.GetString(10);
                        Trabajador.TipoEmpl = odr.IsDBNull(11) ? "" : odr.GetString(11);
                        Trabajador.CodiCeco = odr.IsDBNull(12) ? "" : odr.GetString(12);
                        Trabajador.CodiSede = odr.IsDBNull(13) ? "" : odr.GetString(13);
                        Trabajador.CodiPues = odr.IsDBNull(14) ? "" : odr.GetString(14);
                        Trabajador.CodiArea = odr.IsDBNull(15) ? "" : odr.GetString(15);
                        Trabajador.CodiSuba = odr.IsDBNull(16) ? "" : odr.GetString(16);
                        Trabajador.CodiCelu = odr.IsDBNull(17) ? "" : odr.GetString(17);
                        Trabajador.CodiSind = odr.IsDBNull(18) ? "" : odr.GetString(18);
                        Trabajador.CodiDepa = odr.IsDBNull(19) ? "" : odr.GetString(19);
                        Trabajador.CodiPrvi = odr.IsDBNull(20) ? "" : odr.GetString(20);
                        Trabajador.CodiDist = odr.IsDBNull(21) ? "" : odr.GetString(21);
                        Trabajador.CodiGruph = odr.IsDBNull(22) ? "" : odr.GetString(22);
                        Trabajador.FechaNac = odr.IsDBNull(23) ? "" : odr.GetString(23);
                        Trabajador.TipoDoc = odr.IsDBNull(24) ? "" : odr.GetString(24);
                        Trabajador.NroDoc = odr.IsDBNull(25) ? "" : odr.GetString(25);
                        Trabajador.Sexo = odr.IsDBNull(26) ? "" : odr.GetString(26);
                        Trabajador.Direccion = odr.IsDBNull(27) ? "" : odr.GetString(27);
                        Trabajador.CodiNied = odr.IsDBNull(28) ? "" : odr.GetString(28);
                        Trabajador.CodiEspe = odr.IsDBNull(29) ? "" : odr.GetString(29);
                        Trabajador.FechaIngreso = odr.IsDBNull(30) ? "" : odr.GetString(30);
                        Trabajador.FechaCese = odr.IsDBNull(31) ? "" : odr.GetString(31);
                        Trabajador.CodiEssalud = odr.IsDBNull(32) ? "" : odr.GetString(32);
                        Trabajador.CodiAfp = odr.IsDBNull(33) ? "" : odr.GetString(33);
                        Trabajador.GrupoSang = odr.IsDBNull(34) ? "" : odr.GetString(34);
                        Trabajador.TelefonoFijo = odr.IsDBNull(35) ? "" : odr.GetString(35);
                        Trabajador.TelefonoMovil = odr.IsDBNull(36) ? "" : odr.GetString(36);
                        Trabajador.FlagDescSusti = odr.IsDBNull(37) ? "" : odr.GetString(37);
                        Trabajador.FlagMovilidad = odr.IsDBNull(38) ? "" : odr.GetString(38);
                        Trabajador.Observacion = odr.IsDBNull(39) ? "" : odr.GetString(39);
                        Trabajador.Email = odr.IsDBNull(40) ? "" : odr.GetString(40);
                        Trabajador.UserRed = odr.IsDBNull(41) ? "" : odr.GetString(41);
                        Trabajador.NivelAprobWf = odr.IsDBNull(42) ? "" : odr.GetString(42);
                        Trabajador.CodiCome = odr.IsDBNull(43) ? "" : odr.GetString(43);
                        Trabajador.Jerarquia = odr.IsDBNull(44) ? "" : odr.GetString(44);
                        Trabajador.Foto = odr.IsDBNull(45) ? "" : odr.GetString(45);
                        Trabajador.FlagTarjCome = odr.IsDBNull(46) ? "" : odr.GetString(46);
                        Trabajador.CodiPusa = odr.IsDBNull(47) ? "" : odr.GetString(47);
                        Trabajador.Elegibles = odr.IsDBNull(48) ? "" : odr.GetString(48);
                        Trabajador.Estado = odr.IsDBNull(49) ? "" : odr.GetString(49);
                        Trabajador.SecuenciaHorario = odr.IsDBNull(50) ? "" : odr.GetString(50);
                        Trabajador.RpcEmpresa = odr.IsDBNull(51) ? "" : odr.GetString(51);
                        Trabajador.AnexoEmpresa = odr.IsDBNull(52) ? "" : odr.GetString(52);
                        Trabajador.CodiMorg = odr.IsDBNull(53) ? "" : odr.GetString(53);
                        Trabajador.NroCertificado = odr.IsDBNull(54) ? "" : odr.GetString(54);
                        Trabajador.CodiSeguAfp = odr.IsDBNull(55) ? "" : odr.GetString(55);                        

                        List.Add(Trabajador);
                        Trabajador = null;
                    }
                    odr.Close();
                    ObjConn.Close();
                }
                return List;
            }
            catch
            {
                throw;
            }
        }
        public ResponseSincronizacion InsertUpdate_TrabajadorToSql07(Trabajador_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SQL07"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Trabajador_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiTrab", Entry.CodiTrab));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FichaSap", Entry.FichaSap));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TarjetaHid", Entry.TarjetaHid));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@ApellidoPaterno", Entry.ApellidoPaterno));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@ApellidoMaterno", Entry.ApellidoMaterno));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Nombres", Entry.Nombres));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FichaTareo", Entry.FichaTareo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FichaAntigua", Entry.FichaAntigua));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGere", Entry.CodiGere));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiRela", Entry.CodiRela));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiMoco", Entry.CodiMoco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TipoEmpl", Entry.TipoEmpl));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCeco", Entry.CodiCeco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSede", Entry.CodiSede));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiPues", Entry.CodiPues));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSuba", Entry.CodiSuba));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCelu", Entry.CodiCelu));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSind", Entry.CodiSind));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiDepa", Entry.CodiDepa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiPrvi", Entry.CodiPrvi));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiDist", Entry.CodiDist));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGruph", Entry.CodiGruph));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FechaNac", Entry.FechaNac));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TipoDoc", Entry.TipoDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@NroDoc", Entry.NroDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Sexo", Entry.Sexo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Direccion", Entry.Direccion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiNied", Entry.CodiNied));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiEspe", Entry.CodiEspe));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FechaIngreso", Entry.FechaIngreso));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FechaCese", Entry.FechaCese));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiEssalud", Entry.CodiEssalud));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiAfp", Entry.CodiAfp));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@GrupoSang", Entry.GrupoSang));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TelefonoFijo", Entry.TelefonoFijo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TelefonoMovil", Entry.TelefonoMovil));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FlagDescSusti", Entry.FlagDescSusti));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FlagMovilidad", Entry.FlagMovilidad));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Observacion", Entry.Observacion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Email", Entry.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@UserRed", Entry.UserRed));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@NivelAprobWf", Entry.NivelAprobWf));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCome", Entry.CodiCome));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Jerarquia", Entry.Jerarquia));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Foto", Entry.Foto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FlagTarjCome", Entry.FlagTarjCome));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiPusa", Entry.CodiPusa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Elegibles", Entry.Elegibles));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@SecuenciaHorario", Entry.SecuenciaHorario));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@RpcEmpresa", Entry.RpcEmpresa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@AnexoEmpresa", Entry.AnexoEmpresa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiMorg", Entry.CodiMorg));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@NroCertificado", Entry.NroCertificado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSeguAfp", Entry.CodiSeguAfp));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_TrabajadorToSiderPrd01(Trabajador_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SIDERPRD01"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Trabajador_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiTrab", Entry.CodiTrab));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FichaSap", Entry.FichaSap));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TarjetaHid", Entry.TarjetaHid));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@ApellidoPaterno", Entry.ApellidoPaterno));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@ApellidoMaterno", Entry.ApellidoMaterno));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Nombres", Entry.Nombres));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FichaTareo", Entry.FichaTareo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FichaAntigua", Entry.FichaAntigua));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGere", Entry.CodiGere));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiRela", Entry.CodiRela));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiMoco", Entry.CodiMoco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TipoEmpl", Entry.TipoEmpl));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCeco", Entry.CodiCeco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSede", Entry.CodiSede));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiPues", Entry.CodiPues));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSuba", Entry.CodiSuba));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCelu", Entry.CodiCelu));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSind", Entry.CodiSind));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiDepa", Entry.CodiDepa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiPrvi", Entry.CodiPrvi));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiDist", Entry.CodiDist));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGruph", Entry.CodiGruph));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FechaNac", Entry.FechaNac));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TipoDoc", Entry.TipoDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@NroDoc", Entry.NroDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Sexo", Entry.Sexo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Direccion", Entry.Direccion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiNied", Entry.CodiNied));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiEspe", Entry.CodiEspe));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FechaIngreso", Entry.FechaIngreso));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FechaCese", Entry.FechaCese));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiEssalud", Entry.CodiEssalud));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiAfp", Entry.CodiAfp));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@GrupoSang", Entry.GrupoSang));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TelefonoFijo", Entry.TelefonoFijo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@TelefonoMovil", Entry.TelefonoMovil));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FlagDescSusti", Entry.FlagDescSusti));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FlagMovilidad", Entry.FlagMovilidad));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Observacion", Entry.Observacion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Email", Entry.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@UserRed", Entry.UserRed));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@NivelAprobWf", Entry.NivelAprobWf));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCome", Entry.CodiCome));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Jerarquia", Entry.Jerarquia));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Foto", Entry.Foto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@FlagTarjCome", Entry.FlagTarjCome));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiPusa", Entry.CodiPusa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Elegibles", Entry.Elegibles));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@SecuenciaHorario", Entry.SecuenciaHorario));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@RpcEmpresa", Entry.RpcEmpresa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@AnexoEmpresa", Entry.AnexoEmpresa));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiMorg", Entry.CodiMorg));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@NroCertificado", Entry.NroCertificado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSeguAfp", Entry.CodiSeguAfp));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public List<Gerencias_Model> ListarGerencias()
        {
            try
            {
                OracleConnection ObjConn = Helpers.ConnectToOracle("PERSONAS");
                List<Gerencias_Model> List = null;
                if (ObjConn != null)
                {
                    ObjConn.Open();
                    OracleDataReader odr = null;
                    OracleCommand command = ObjConn.CreateCommand();

                    List = new List<Gerencias_Model>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Sp_List_Sincronizacion_Gerencias";
                    //command.Parameters.Add(new OracleParameter("pn_codi_trab", entry.CodiTrab));
                    command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    odr = command.ExecuteReader();
                    while (odr.Read())
                    {
                        Gerencias_Model Entity = new Gerencias_Model();
                        Entity.CodiGere = odr.IsDBNull(0) ? "" : odr.GetString(0);
                        Entity.Descripcion = odr.IsDBNull(1) ? "" : odr.GetString(1);
                        Entity.JefeGerencia = odr.IsDBNull(2) ? "" : odr.GetString(2);
                        Entity.Estado = odr.IsDBNull(3) ? "" : odr.GetString(3);
                        Entity.DescripcionCorta = odr.IsDBNull(4) ? "" : odr.GetString(4);
                        Entity.Color = odr.IsDBNull(5) ? "" : odr.GetString(5);

                        List.Add(Entity);
                        Entity = null;
                    }
                    odr.Close();
                    ObjConn.Close();
                }
                return List;
            }
            catch
            {
                throw;
            }
        }
        public List<Areas_Model> ListarAreas()
        {
            try
            {
                OracleConnection ObjConn = Helpers.ConnectToOracle("PERSONAS");
                List<Areas_Model> List = null;
                if (ObjConn != null)
                {
                    ObjConn.Open();
                    OracleDataReader odr = null;
                    OracleCommand command = ObjConn.CreateCommand();

                    List = new List<Areas_Model>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Sp_List_Sincronizacion_Areas";
                    //command.Parameters.Add(new OracleParameter("pn_codi_trab", entry.CodiTrab));
                    command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    odr = command.ExecuteReader();
                    while (odr.Read())
                    {
                        Areas_Model Entity = new Areas_Model();
                        Entity.CodiArea = odr.IsDBNull(0) ? "" : odr.GetString(0);
                        Entity.CodiGere = odr.IsDBNull(1) ? "" : odr.GetString(1);
                        Entity.Descripcion = odr.IsDBNull(2) ? "" : odr.GetString(2);
                        Entity.JefeArea = odr.IsDBNull(3) ? "" : odr.GetString(3);
                        Entity.Estado = odr.IsDBNull(4) ? "" : odr.GetString(4);
                        Entity.DescripcionCorta = odr.IsDBNull(5) ? "" : odr.GetString(5);
                        Entity.Color = odr.IsDBNull(6) ? "" : odr.GetString(6);
                        Entity.CodiCeco = odr.IsDBNull(7) ? "" : odr.GetString(7);

                        List.Add(Entity);
                        Entity = null;
                    }
                    odr.Close();
                    ObjConn.Close();
                }
                return List;
            }
            catch
            {
                throw;
            }
        }
        public List<Subareas_Model> ListarSubareas()
        {
            try
            {
                OracleConnection ObjConn = Helpers.ConnectToOracle("PERSONAS");
                List<Subareas_Model> List = null;
                if (ObjConn != null)
                {
                    ObjConn.Open();
                    OracleDataReader odr = null;
                    OracleCommand command = ObjConn.CreateCommand();

                    List = new List<Subareas_Model>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Sp_List_Sincronizacion_Subareas";
                    //command.Parameters.Add(new OracleParameter("pn_codi_trab", entry.CodiTrab));
                    command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    odr = command.ExecuteReader();
                    while (odr.Read())
                    {
                        Subareas_Model Entity = new Subareas_Model();
                        Entity.CodiSuba = odr.IsDBNull(0) ? "" : odr.GetString(0);
                        Entity.CodiArea = odr.IsDBNull(1) ? "" : odr.GetString(1);
                        Entity.Descripcion = odr.IsDBNull(2) ? "" : odr.GetString(2);
                        Entity.JefeSubarea = odr.IsDBNull(3) ? "" : odr.GetString(3);
                        Entity.Estado = odr.IsDBNull(4) ? "" : odr.GetString(4);
                        Entity.DescripcionCorta = odr.IsDBNull(5) ? "" : odr.GetString(5);
                        Entity.Color = odr.IsDBNull(6) ? "" : odr.GetString(6);

                        List.Add(Entity);
                        Entity = null;
                    }
                    odr.Close();
                    ObjConn.Close();
                }
                return List;
            }
            catch
            {
                throw;
            }
        }
        public List<Celulas_Model> ListarCelulas()
        {
            try
            {
                OracleConnection ObjConn = Helpers.ConnectToOracle("PERSONAS");
                List<Celulas_Model> List = null;
                if (ObjConn != null)
                {
                    ObjConn.Open();
                    OracleDataReader odr = null;
                    OracleCommand command = ObjConn.CreateCommand();

                    List = new List<Celulas_Model>();

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Sp_List_Sincronizacion_Celulas";
                    //command.Parameters.Add(new OracleParameter("pn_codi_trab", entry.CodiTrab));
                    command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                    odr = command.ExecuteReader();
                    while (odr.Read())
                    {
                        Celulas_Model Entity = new Celulas_Model();
                        Entity.CodiCelu = odr.IsDBNull(0) ? "" : odr.GetString(0);
                        Entity.CodiArea = odr.IsDBNull(1) ? "" : odr.GetString(1);
                        Entity.Descripcion = odr.IsDBNull(2) ? "" : odr.GetString(2);
                        Entity.JefeCelula = odr.IsDBNull(3) ? "" : odr.GetString(3);
                        Entity.Estado = odr.IsDBNull(4) ? "" : odr.GetString(4);
                        Entity.DescripcionCorta = odr.IsDBNull(5) ? "" : odr.GetString(5);
                        Entity.Color = odr.IsDBNull(6) ? "" : odr.GetString(6);

                        List.Add(Entity);
                        Entity = null;
                    }
                    odr.Close();
                    ObjConn.Close();
                }
                return List;
            }
            catch
            {
                throw;
            }
        }
        public ResponseSincronizacion InsertUpdate_GerenciasToSql07(Gerencias_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SQL07"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Gerencias_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGere", Entry.CodiGere));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeGerencia", Entry.JefeGerencia));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_AreasToSql07(Areas_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SQL07"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Areas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGere", Entry.CodiGere));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeArea", Entry.JefeArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCeco", Entry.CodiCeco));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_SubareasToSql07(Subareas_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SQL07"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Subareas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSuba", Entry.CodiSuba));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeSubarea", Entry.JefeSubarea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_CelulasToSql07(Celulas_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SQL07"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Celulas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCelu", Entry.CodiCelu));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeCelula", Entry.JefeCelula));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_GerenciasToSiderPrd01(Gerencias_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SIDERPRD01"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Gerencias_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGere", Entry.CodiGere));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeGerencia", Entry.JefeGerencia));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_AreasToSiderPrd01(Areas_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SIDERPRD01"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Areas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiGere", Entry.CodiGere));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeArea", Entry.JefeArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCeco", Entry.CodiCeco));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_SubareasToSiderPrd01(Subareas_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SIDERPRD01"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Subareas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiSuba", Entry.CodiSuba));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeSubarea", Entry.JefeSubarea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_CelulasToSiderPrd01(Celulas_Model Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PERSONAS_SIDERPRD01"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "Sp_Ins_Upd_Celulas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiCelu", Entry.CodiCelu));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodiArea", Entry.CodiArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Descripcion", Entry.Descripcion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@JefeCelula", Entry.JefeCelula));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Estado", Entry.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@DescripcionCorta", Entry.DescripcionCorta));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Color", Entry.Color));

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
    }
}